# How to upgrade to new version

## Example upgrade to v1.8.0

```bash
git add .
git commit -m "update to v1.8.0"
git tag v1.8.0
git push public master v1.8.0
```

## After update change version of Golang and run

```bash
go mod tidy
```
