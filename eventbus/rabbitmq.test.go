package pkgEventBus

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	pkgDomain "gitlab.com/h162/golang-utils/domain"
)


func TestFullFlow(eventBus EventBusInterface, ctx context.Context){
	go func(){
		handler := func(ctx context.Context, event *pkgDomain.Event) error {
			fmt.Println("======================Event Handler======================")
	
			fmt.Println(event.Payload)
	
			return nil
		}

		handlerByEvent := make(map[string]EventHandler)
		handlerByEvent["db_cards_created"] = handler

		if err := eventBus.Subscribe(ctx, handlerByEvent); err != nil {
			fmt.Println("Cannot subscribe to err Bus")
		}
	}()

	go func(){
		eventID, errEventCreated := uuid.NewRandom()

		if errEventCreated != nil {
			logrus.New().Error("Create event id err: ", errEventCreated)
			// return pkgError.Wrap(errEventCreated)
		}

		domainEvent := pkgDomain.Event{
			ID: eventID,
			Type: "db_cards_created",
			StreamID: eventID,
			StreamName: "POST_MANAGEMENT",
			OccurredAt: time.Now(),
			Payload: []byte("cards Payload"),
		}

		eventBus.Publish(ctx, &domainEvent, true)
	}()


}