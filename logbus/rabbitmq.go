package pkgLogBus

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"

	pkgDomain "gitlab.com/h162/golang-utils/domain"
	pkgError "gitlab.com/h162/golang-utils/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type LogBusRabbitMQ struct {
	logger     *logrus.Logger
	channel *amqp.Channel
	mainQueue amqp.Queue
	callBackQueue amqp.Queue
	consumer <-chan amqp.Delivery
	callBackConsumer <-chan amqp.Delivery
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func randomString(l int) string {
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
			bytes[i] = byte(randInt(65, 90))
	}
	return string(bytes)
}


func NewRabbitMQ(host string, port string, usename string, password string) LogBusInterface{
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%v:%v@%v:%v/", usename, password, host, port))
	
	if err != nil{
		logrus.New().Error("Failed to connect to RabbitMQ")
		panic(err)
	}

	ch, err := conn.Channel()

	if err != nil{
		logrus.New().Error("Failed to open a channel")
		panic(err)
	}

	err = ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)

	if err != nil{
		logrus.New().Error("Failed to set QoS")
		panic(err)
	}

	mainQueue, err := ch.QueueDeclare(
		"event-bus", // name
		false,          // durable
		false,          // delete when unused
		false,          // exclusive
		false,          // no-wait
		nil,            // arguments
	)

	if err != nil{
		logrus.New().Error("Failed to declare a queue")
		panic(err)
	}

	callBackQueue, err := ch.QueueDeclare(
		"event-bus-callback", // name
		false,          // durable
		false,          // delete when unused
		false,          // exclusive
		false,          // no-wait
		nil,            // arguments
	)

	if err != nil{
		logrus.New().Error("Failed to declare a callback queue")
		panic(err)
	}

	return &LogBusRabbitMQ{
		logger: logrus.New(),
		mainQueue: mainQueue,
		callBackQueue: callBackQueue,
		channel: ch,
	}
}

func (bus *LogBusRabbitMQ) RegisterCallBack(){
	callBackConsumer, err := bus.channel.Consume(
		bus.callBackQueue.Name, // queue
		"callBackConsumer",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	if err != nil{
		logrus.New().Error("Failed to register a callback queue")
	}

	bus.callBackConsumer = callBackConsumer
}

func (bus *LogBusRabbitMQ) Publish(parentCtx context.Context, event *pkgDomain.Event, callback bool) error {	
	eventPayload, errParse := json.Marshal(event)

	if errParse != nil {return pkgError.Wrap(errParse)}

	corrId := randomString(32)

	if err:= bus.channel.Publish(
		"",
		bus.mainQueue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			CorrelationId: corrId,
			Body: eventPayload,
			ReplyTo: bus.callBackQueue.Name,
		},
	); err != nil {
		bus.logger.Error("[EventBus - Publisher] Error in publish ", err)
		return pkgError.Wrap(err)
	}

	bus.logger.Info("[EventBus - Publisher] Published a message: ", event.Payload)

	if callback {
		var errLogicString string

		for d := range bus.callBackConsumer {
			if corrId == d.CorrelationId {
				bus.logger.Info("[EventBus - Publisher] Recevied response from subscriber")
				errLogicString = string(d.Body[:])
				break
			}
	
			bus.logger.Info("[EventBus - Publisher] Ignore mismatch response from subscriber")
		}
	
		// bus.channel.Cancel("callBackConsumer", false)
	
		if errLogicString != "" {
			return pkgError.New(errLogicString)
		}	
	}

	return nil
}


func (bus *LogBusRabbitMQ) Subscribe(ctx context.Context, handlerByEvent map[string]LogHandler) error{
	consumer, err := bus.channel.Consume(
		bus.mainQueue.Name,
		"",
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	if err != nil{
		logrus.New().Error("Failed to register a consumer")
	}

	bus.consumer = consumer

	go func() {
		for d := range bus.consumer {
			var domainEvent pkgDomain.Event
	
			if err := json.Unmarshal(d.Body, &domainEvent); err != nil{
				bus.logger.Error(fmt.Sprintf("[EventBus - Subscriber] Error in parsing: %v", err))
			}

			bus.logger.Info("Action by Event ", handlerByEvent)

			bus.logger.Info(fmt.Sprintf("[EventBus - Subcriber] Received a event: %v with payload %v", domainEvent.Type, domainEvent.Payload))

			if handler, event := handlerByEvent[domainEvent.Type]; event {
				errLogicString := ""

				if errLogic := handler(ctx, &domainEvent); errLogic != nil {
					bus.logger.Error("[EventBus - Subscriber] ", errLogic)
					errLogicString = errLogic.Error()
				}

				if err := bus.channel.Publish(
					"",        // exchange
					d.ReplyTo, // routing key
					false,     // mandatory
					false,     // immediate
					amqp.Publishing{
							ContentType:   "application/json",
							CorrelationId: d.CorrelationId,
							Body:          []byte(errLogicString),
				}); err != nil {
					bus.logger.Error(fmt.Sprintf("[EventBus - Subscriber] Error in publish: %v", err))
				}

				bus.logger.Info("[EventBus - Subcriber] Published response message")
			}
		}
	}()

	return nil
}
